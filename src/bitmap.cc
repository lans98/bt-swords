#include "bitmap.h"

Bitmap::Bitmap(const char* file) {
  bitmap = al_load_bitmap(file);
  pos_x = 0;
  pos_y = 0;
  source_x = 0;
  source_y = 0;
  destroyed = false;
}

Bitmap::~Bitmap() {
  if (bitmap) al_destroy_bitmap(bitmap);
}

const double Bitmap::GetX() { return pos_x; }
const double Bitmap::GetY() { return pos_y; }

void Bitmap::SetX(const double& x) { pos_x = x; }
void Bitmap::SetY(const double& y) { pos_y = y; }

const int Bitmap::GetWidth() { return width; }
const int Bitmap::GetHeight() { return height; }

void Bitmap::SetSourceX(const int& x) { source_x = x; }
void Bitmap::SetSourceY(const int& y) { source_y = y; }

void Bitmap::SetDestroyed(const bool& state) { destroyed = state; }
const bool Bitmap::IsDestroyed() { return destroyed; }

void Bitmap::ChangeBitmap(const char* new_file) {
  al_destroy_bitmap(bitmap);
  bitmap = al_load_bitmap(new_file);
}
