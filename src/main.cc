#include <btqlib/bluez.h>
#include <btqlib/client.h>
#include <btqlib/inquiry_service.h>

#include "game.h"

using namespace std;
using namespace bluez;

int main(int argc, char *argv[]) {
  InquiryService *inq_service;
  try {
    inq_service = new InquiryService();
  } catch(CantOpenDeviceOrSocket e) {
    cout << e.what() << endl;
    return 1;
  }
  inq_service->Search();
  inq_service->ShowResults();
  cout << "-> Choose two devices: ";
  int fth_one, snd_one;
  cin >> fth_one >> snd_one;
  BTClient *me = new BTClient;
  me->AddNewMate(inq_service->AddressOf(fth_one));
  me->AddNewMate(inq_service->AddressOf(snd_one));

  // We don't need it anymore
  delete inq_service;

  // Try to connect to both new devices
  while (true) {
    try {
      me->ConnectTo(0);
      me->ConnectTo(1);
    } catch (ConnectionFailed e) {
      cout << e.what() << endl;
      char selection = 'y';
      cout << "Do you want to try again? (y/n) ";
      while (getchar() != '\n');
      selection = getchar();
      if (selection == 'Y' || selection == 'y' || selection == '\n')
        continue;
      else
        return 1;
    }
    break;
  }

  FrameworkGame game(me);
  game.StartTimer();
  while(!game.GetGameActivityState()) {
    AllegroEvent event;
    game.WaitForEvent(event);
    game.ManageEvent(event);
  }
  return 0;
}
