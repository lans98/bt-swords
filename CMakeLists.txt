cmake_minimum_required(VERSION 3.6)
project(Swords)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "-O2 -pthread")
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

file(GLOB SOURCES "src/*.cc")
include_directories(include)

find_library(BTFOUND bluetooth)
find_library(BTQFOUND btqlib)

if (BTFOUND AND BTQFOUND)
  add_executable(Swords ${SOURCES})
  target_link_libraries(Swords
    btqlib
    bluetooth
    allegro_acodec
    allegro_audio
    allegro_color
    allegro_dialog
    allegro_image
    allegro_main
    allegro_memfile
    allegro_physfs
    allegro_primitives
    allegro_ttf
    allegro_font
    allegro)

endif()
