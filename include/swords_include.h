#ifndef SWORDS_SWORDS_INCLUDE_H
#define SWORDS_SWORDS_INCLUDE_H

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_native_dialog.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

#include <cstdio>
#include <cstdlib>

#include <btqlib/bluez.h>
#include <btqlib/client.h>
#include <btqlib/inquiry_service.h>

#include "exceptions.h"

using AllegroDisplay = ALLEGRO_DISPLAY;
using AllegroEventQueue = ALLEGRO_EVENT_QUEUE;
using AllegroTimer = ALLEGRO_TIMER;
using AllegroEvent = ALLEGRO_EVENT;
using AllegroBitmap = ALLEGRO_BITMAP;
using AllegroFont = ALLEGRO_FONT;

struct ScreenDisplay {
  int width, height;
  int fps;
};

const static ScreenDisplay screen = {640, 170, 60};

#endif
