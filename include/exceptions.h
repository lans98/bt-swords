#ifndef SWORDS_EXCEPTIONS_H
#define SWORDS_EXCEPTIONS_H

#include <exception>

class AnotherFrameworkGameInstation : public std::exception {
public:
  const char* what() const noexcept {
    return "There is another framework game instation, fatal error!";
  }
};

#endif
